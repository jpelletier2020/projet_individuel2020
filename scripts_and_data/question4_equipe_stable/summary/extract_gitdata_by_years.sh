git log --all --numstat --date=short --pretty=format:'--%h--%ad--%aN' --no-renames --before=2014-01-01 > gitdata_2013year.log
git log --all --numstat --date=short --pretty=format:'--%h--%ad--%aN' --no-renames --before=2015-01-01 --after=2013-12-31 > gitdata_2014year.log
git log --all --numstat --date=short --pretty=format:'--%h--%ad--%aN' --no-renames --before=2016-01-01 --after=2014-12-31 > gitdata_2015year.log
git log --all --numstat --date=short --pretty=format:'--%h--%ad--%aN' --no-renames --before=2017-01-01 --after=2015-12-31 > gitdata_2016year.log
git log --all --numstat --date=short --pretty=format:'--%h--%ad--%aN' --no-renames --before=2018-01-01 --after=2016-12-31 > gitdata_2017year.log
git log --all --numstat --date=short --pretty=format:'--%h--%ad--%aN' --no-renames --before=2019-01-01 --after=2017-12-31 > gitdata_2018year.log
git log --all --numstat --date=short --pretty=format:'--%h--%ad--%aN' --no-renames --before=2020-01-01 --after=2018-12-31 > gitdata_2019year.log
git log --all --numstat --date=short --pretty=format:'--%h--%ad--%aN' --no-renames --before=2021-01-01 --after=2019-12-31 > gitdata_2020year.log