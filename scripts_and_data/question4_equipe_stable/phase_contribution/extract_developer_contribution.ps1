$path=$args[0]
docker run --rm -v ${path}:/data code-maat-app -c git2 -l /data/gitdata_first_phase.log -a author-churn > author_churn_first_phase.csv
docker run --rm -v ${path}:/data code-maat-app -c git2 -l /data/gitdata_second_phase.log -a author-churn > author_churn_second_phase.csv