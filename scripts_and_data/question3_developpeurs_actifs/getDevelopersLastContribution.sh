#!/bin/bash

input=$1
developers=( "Jason Gustafson"
             "Matthias J. Sax"
			 "Edward Jay Kreps"
			 "Jay Kreps"
			 "Guozhang Wang"
			 "Ewen Cheslack-Postava"
			 "Damian Guy"
			 "Jun Rao"
			 "Colin P. Mccabe"
			 "Colin Patrick McCabe"
			 "Rajini Sivaram"
			 "Ismael Juma"
			 "Joe Stein"
			 "John Roesler"
			 "Neha Narkhede"
			 "Gavin McDonald"
			 "Bill Bejeck"
			 "A. Sophie Blee-Goldman"
			 "Randall Hauch"
			 "Boyang Chen"
			 "Konstantine Karantasis"
			 "Eno Thereska" )

for developer in "${developers[@]}"
do
	while read line;
	do
	  if echo "$line" | grep -q "$developer";
	  then
		echo "$line";
		break;
	  fi	
	done < $1
done