#!/bin/bash

wordToFind=$1
inputFile=$2

linesFound=$(grep "$1" -c $2)

totalLines=$(grep "" -c $2)

echo "Lines found : $linesFound" 
echo "Total Lines : $totalLines"