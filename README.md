# Projet Individuel MGL7460-90 (Hiver 2020)

  * Auteur : Jimmy Pelletier
  * Date de publication du rapport intermédiaire : 2020-03-01
  * Date de publication du rapport finale : 2020-04-12
  * Rapport intermédiaire disponible : [Lien](https://gitlab.com/jpelletier2020/projet_individuel2020/-/blob/master/report/v1/ProjetIndividuelV1.pdf)
  * Rapport finale disponible : [Lien](https://gitlab.com/jpelletier2020/projet_individuel2020/-/blob/master/report/final/FinalReport.pdf)

## Objectifs pédagogiques
Dans ce projet individuel, vous êtes en chargé par votre hiérarchie de l’analyse
d’un logiciel légataire afin d’estimer le risque pour votre compagnie d’accepter
un projet de tierce maintenance applicative sur ce logiciel. Pour rendre cette situation réaliste, nous simulerons
 cette application légataire par une application open-source accessible publiquement.

Il est a votre charge d’identifier la méthodologie à mettre en oeuvre pour mener
l’analyse de ce logiciel, de la mettre en oeuvre, et de livrer vos conclusions
vis à vis de la “maintenabilité” du logiciel étudié sous la forme d’un rapport de synthèse.

## Sujet choisi
Apache Kafka

Dépôt disponible sur GitHub : [GitHub Apache Kafka](https://github.com/apache/kafka)

## Cadre d'analyse de maintenance

Les scripts et les données générées pour écrire le rapport sont disponible sous le répertoire scripts_and_data 
et chacune des questions qui ont nécessitées des script/données sont décrites ci-bas si vous voulez les réutilisés.

Les thématiques suivantes seront discutés dans l'analyse :

### Dimension "Équipe de développement"

1- Qui sont les développeurs principaux du projet?

    Étapes pour reproduire les données permettant de répondre à cette question :
  
    1. Cloner le dépôt suivant : https://github.com/adamtornhill/code-maat
    2. Construire une image docker de code-maat comme écrit dans le README.md du dépôt
    3. Extraire les données git du dépôt d’Apache Kafka :
      a. Se positionner à la racine du dépôt d'Apache Kafka et exécuter le script extract_gitdata.sh
    4. Exécuter une analyse sommaire du dépôt avec l’image docker code-maat :
      a. Exécuter le script PowerShell extract_git_summary.ps1 avec un argument spécifiant le répertoire
         où se trouve le fichier gitdata.log de l’étape 3
    5. Exécuter l’analyse de contributions par développeur avec l’image docker code-maat :        
      a. Exécuter le script PowerShell extract_contributors.ps1 avec un argument spécifiant le répertoire
         où se trouve le fichier gitdata.log de l’étape 3      
  
2- Est-ce que les développeurs principaux ont un lien entre eux?

    Étapes pour reproduire les données permettant de répondre à cette question :

    N/A

3- Est-ce que les développeurs principaux sont encore actifs?
  
    Étapes pour reproduire les données permettant de répondre à cette question :
   
    1. Suppression de contenu indésirables dans le git log
      a. Exécuter le script simplify_gitdata.sh
    2. Générer le rapport indiquant le dernier commit des développeurs principaux
      a. Exécuter le script getDevelopersLastContribution.sh
    3. Étant donné les deux auteurs différents avec deux noms, il faut manuellement sélectionner la date de
       commit la plus récente pour ces deux auteurs.

4- L'équipe de développement est-elle stable?
  
    Étapes pour reproduire les données permettant de construire le premier graphique :
 
    1. Extraire les données git log pour chacune des années
      a. Exécuter le script extract_gitdata_by_years.sh
    2. Exécuter la commande summary de code maat sur chacun des git log
      a. Exécuter le script PowerShell extract_summary.ps1 avec un argument spécifiant le répertoire où
         se trouve les fichiers de l’étape 1
    3. Mise en commun manuelle de chacune des années dans Excel
    
    Étapes pour reproduire les données permettant de construire le deuxième graphique : 
 
    1. Extraire les données git log pour les deux vagues
      a. Exécuter le script extract_gitdata_by_phase.sh
    2. Exécuter l’analyse du nombre de contribution par développeur
      a. Exécuter le script PowerShell extract_developer_contribution.ps1 avec un argument
         spécifiant le répertoire où se trouve les fichiers de l'étape 1.
    3. Mise en commun manuelle des résultats dans Excel

5- Comment les développeurs communiquent entre eux?

    Étapes pour reproduire les données permettant de répondre à cette question :

    1. Extraire les messages de commit depuis l'existence du dépôt
        a. Se positionner à la racine du dépôt d'Apache Kafka et exécuter le script extract_gitlog_message.sh
    2. Exécuter le script find_percentage_of_line_containing_a_word_in_a_file.sh
        a. Exemple : 
          i.  sh find_lines_containing_a_word_in_a_file.sh kafka- gitlog_message.log > kafka_lowercase.csv
          ii. sh find_lines_containing_a_word_in_a_file.sh KAFKA- gitlog_message.log > kafka_uppercase.csv
    3. Mise en commun manuelle des résultats dans Excel

### Dimension "Code source"

6- Comment qualifiez-vous la qualité du code source?

    Étapes pour reproduire les données permettant de répondre à cette question :

    N/A

### Dimension "Tests"

7- Quelles sont les méthodes de tests mise en place dans le projet?

    Étapes pour reproduire les données permettant de répondre à cette question :

    N/A

8- Comment qualifiez-vous la qualité des tests mis en oeuvre ?

    Étapes pour reproduire les données permettant de répondre à cette question :

    N/A